<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EmployeesController;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Employee Test Case
 */
class addEmployeeTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Employees',
        'app.Departments'
    ];

    public function setUp()
    {
        parent::setUp();
        $this->enableCsrfToken();
        $this->enableSecurityToken();
        $this->enableRetainFlashMessages();
    }
    /**
     * Test add success
     *
     * @return void
     */
    public function testAddSuccess()
    {
        $data = [
            'first_name' => 'test',
            'last_name' => 'test',
            'birthday' => '2000-01-01',
            'department_id' => 1
        ];
        $this->post('/employees/add', $data);

        $this->assertResponseSuccess();
        $employees = TableRegistry::getTableLocator()->get('Employees');
        $this->assertEquals(2, $employees->find()->count());
        $this->assertFlashMessage('The employee has been saved.');
    }
    /**
     * Test add failed
     *
     * @return void
     */
    public function testAddFailedMultipleValidation()
    {
        $data = [
            'first_name' => null,
            'last_name' => null,
            'birthday' => '2000-01-01',
            'department_id' => 1
        ];
        $this->post('/employees/add', $data);

        $this->assertResponseSuccess();
        $employees = TableRegistry::getTableLocator()->get('Employees');
        $this->assertEquals(1, $employees->find()->count());
        $viewEmployee = $this->viewVariable('employee')->getErrors();
        // expected validation message
        $expectedValidationMessages = [
            'first_name' => [
                'This field cannot be left empty'
            ],
            'last_name' => [
                'This field cannot be left empty'
            ]
        ];

        foreach ($viewEmployee as $key => $value) {
            $this->assertEquals(true, in_array(array_values($value)[0], $expectedValidationMessages[$key]));
        }
        $this->assertFlashMessage('The employee could not be saved. Please, try again.');
    }

    /**
     * Test add failed
     *
     * @return void
     */
    public function testAddFailedBirthdayDateInvalid()
    {
        $data = [
            'first_name' => 'test',
            'last_name' => 'test',
            'birthday' => 'aaa',
            'department_id' => 1
        ];
        $this->post('/employees/add', $data);

        $this->assertResponseSuccess();
        $employees = TableRegistry::getTableLocator()->get('Employees');
        $this->assertEquals(1, $employees->find()->count());
        $viewEmployee = $this->viewVariable('employee')->getErrors();
        // expected validation message
        $expectedValidationMessages = [
            'birthday' => [
                'The provided value is invalid'
            ]
        ];
        foreach ($viewEmployee as $key => $value) {
            $this->assertEquals(true, in_array(array_values($value)[0], $expectedValidationMessages[$key]));
        }
        $this->assertFlashMessage('The employee could not be saved. Please, try again.');
    }
    
    /**
     * Test add failed
     *
     * @return void
     */
    public function testAddFailedForeignKey()
    {
        $data = [
            'first_name' => 'test',
            'last_name' => 'test',
            'birthday' => '2000-01-01',
            'department_id' => 2
        ];
        $this->post('/employees/add', $data);

        $this->assertResponseSuccess();
        $employees = TableRegistry::getTableLocator()->get('Employees');
        $this->assertEquals(1, $employees->find()->count());
        $viewEmployee = $this->viewVariable('employee')->getErrors();
        // expected validation message
        // department_id is not existing in Departments table
        $expectedValidationMessages = [
            'department_id' => [
                'This value does not exist'
            ]
        ];
        foreach ($viewEmployee as $key => $value) {
            $this->assertEquals(true, in_array(array_values($value)[0], $expectedValidationMessages[$key]));
        }
        $this->assertFlashMessage('The employee could not be saved. Please, try again.');
    }
    public function tearDown()
    {
        parent::tearDown();
    }
}
